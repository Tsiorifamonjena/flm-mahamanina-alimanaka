import moment from 'moment';

export function initializeLocale() {
    moment.defineLocale('mg', {parentLocale: 'fr'});
    moment.locale('mg', {
        months: [
            "Janoary", "Febroary", "Martsa", "Aprily", "May", "Jona", "Jolay", "Aogositra", "Septambra", "Oktobra", "Novambra", "Desambra"
        ],
        weekdays: [
            "Alahady", "Alatsinainy", "Talata", "Alarobia", "Alakamisy", "Zoma", "Sabotsy"
        ],
        weekdaysShort: ["Alh", "Alts", "Tal", "Alr", "Alk", "Zom", "Sab"]
    });
}
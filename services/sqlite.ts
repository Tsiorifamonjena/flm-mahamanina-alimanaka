import * as SQLite from 'expo-sqlite';
import moment, { Moment } from 'moment';
import { Asset, FileSystem } from 'react-native-unimodules';

const DB_NAME = 'alimanaka.db';
const SQLite_DIR_NAME = 'SQLite';

async function openDatabase(): Promise<SQLite.WebSQLDatabase> {
    if (!(await FileSystem.getInfoAsync(FileSystem.documentDirectory + SQLite_DIR_NAME)).exists) {
        await FileSystem.makeDirectoryAsync(FileSystem.documentDirectory + SQLite_DIR_NAME);

        await FileSystem.downloadAsync(
            Asset.fromModule(require(`../assets/data/${DB_NAME}`)).uri,
            FileSystem.documentDirectory + `${SQLite_DIR_NAME}/${DB_NAME}`
        );
    }

    return SQLite.openDatabase(DB_NAME);
}

export async function findBYDate(date: Moment): Promise<Pericope[]> {
    return new Promise(async (resolve, reject) => {
        const db = await openDatabase();
        const params = moment(date).format('DD-MM-YYYY');

        db.transaction(tx => {
            tx.executeSql(
                'SELECT * FROM perikopa WHERE date = ? LIMIT 1', [params],
                (txObj, content: any) => resolve(content.rows._array),
                (txObj, error) => {
                    reject(error);
                    return false;
                }
            )
        });
    });
}

export function insertPericope(pericope: Pericope): Promise<any> {
    return new Promise(async (resolve, reject) => {
        const db = await openDatabase();
        const { date, verses, description } = pericope;

        db.transaction(tx => {
            tx.executeSql(
                'INSERT INTO perikopa (date, verses, description) VALUES(?, ?, ?)', [date, verses, description],
                (txObj, content: any) => resolve(content.rows._array),
                (txObj, error) => {
                    reject(error);
                    return false;
                }
            )
        });
    });
}

export function updatePericope(pericope: Pericope): Promise<any> {
    return new Promise(async (resolve, reject) => {
        const db = await openDatabase();
        const { ID, verses, description } = pericope;

        db.transaction(tx => {
            tx.executeSql(
                'UPDATE perikopa SET verses = ?, description = ? WHERE ID = ?', [verses, description, ID],
                (txObj, content: any) => resolve(content.rows._array),
                (txObj, error) => {
                    reject(error);
                    return false;
                }
            )
        });
    });
}

export interface Pericope {
    ID?: number;
    date: string;
    description?: string | null;
    verses: string;
}
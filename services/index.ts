import moment, { Moment } from 'moment';

export const getDaysInMonth = (date: Date): number => {
    return new Date(date.getFullYear(), (date.getMonth() + 1), 0).getDate();
};

export const getWeeksInMonth = (date: Date): number => {
    const year = date.getFullYear();
    const month = date.getMonth();
    const first = new Date(year, (month - 1), 1);
    const last = new Date(year, month, 0);

    const used = first.getDay() + last.getDate();

    return Math.ceil(used / 7);
}

export const getWeeksInMonthFromMoment = (date: Moment) => {
    const first = date.startOf('months').format('YYYY-MM-DD');
    const last = date.endOf('months').format('YYYY-MM-DD');

    const used = moment(last, 'YYYY-MM-DD').diff(moment(first, 'YYYY-MM-DD'), 'weeks');

    return moment(last, 'YYYY-MM-DD').day() === 0 || moment(first, 'YYYY-MM-DD').day() === 6 ? used + 2 : used + 1;
}
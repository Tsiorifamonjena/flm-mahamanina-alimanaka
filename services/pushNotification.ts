import moment from 'moment';
import { Platform } from 'react-native';
import PushNotification from 'react-native-push-notification';
import { findBYDate, Pericope } from './sqlite';

const CHANNEL_ID = 'perikopa_channelID';
const CHANNEL_NAME = 'perikopa_channel';

export function createChannel(): Promise<any> {
    return new Promise(resolve => {
        PushNotification.createChannel({
            channelId: CHANNEL_ID,
            channelName: CHANNEL_NAME
        }, (created) => resolve(created));
    });
}

export function localNotificationSchedule() {
    PushNotification.getScheduledLocalNotifications(async notifications => {
        const tomorrow = moment().add(1, 'days');
        const date = new Date(tomorrow.year(), tomorrow.month(), tomorrow.date(), 6, 0, 0);
        const pericope: Pericope = (await findBYDate(tomorrow))[0];

        if (notifications.length === 0 && pericope) {
            PushNotification.localNotificationSchedule({
                channelId: CHANNEL_ID,
                title: moment(tomorrow).format('dddd DD/MM/YYYY'),
                message: pericope.verses + '\n' + (pericope.description && pericope.description),
                subText: pericope.description ? pericope.description : '',
                date
            });
        }
    });
}

export function configure() {
    PushNotification.configure({
        onNotification: () => localNotificationSchedule(),
        requestPermissions: Platform.OS === 'ios'
    });
}
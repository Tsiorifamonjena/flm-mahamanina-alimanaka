import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const Header: React.FC = () => (
    <View style={styles.container}>
        <View style={styles.container_img}>
            <Image style={styles.img} source={require('../assets/rose.svg.png')} />
        </View>
        <View style={{alignItems: 'center'}}>
            <Text style={styles.title_text}>Fiangonana Loterana Malagasy</Text>
            <Text style={styles.title_text}>Mahamanina Fiadanana</Text>
            <Text style={styles.italic_text}>"eto amin'ity fitoerana ity no hanomezako, ny fiadanana." </Text>
            <Text style={styles.italic_text}>Hagay 2.9</Text>
        </View>
    </View>
);

const styles = StyleSheet.create({
    container: {
        margin: 16,
        flexDirection: 'row'
    },
    container_img: {
        flex: 1,
        marginRight: 25
    },
    img: {
        width: 50,
        height: 50
    },
    italic_text: {
        textAlign: 'center',
        fontStyle: 'italic'
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 17
    }
});

export default Header;
import React, { useState } from 'react';
import { Button, StyleSheet, Text, TextInput, View } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { insertPericope, Pericope, updatePericope } from '../services/sqlite';

interface CRUDModalProps {
    modalRef: React.RefObject<Modalize>;
    pericope: Pericope | undefined;
    todaysDate: string;
    onSaved: Function;
}

const CRUDModal: React.FC<CRUDModalProps> = ({modalRef, pericope, todaysDate, onSaved}) => {
    const [verses, setVerses] = useState<string>();
    const [description, setDescription] = useState<any>();

    return(
        <Modalize 
            adjustToContentHeight={true} 
            ref={modalRef} 
            onClose={() => {
                setVerses('');
                setDescription(null);
            }}
            onOpen={() => {
                if (pericope) {
                    setVerses(pericope.verses);
                    setDescription(pericope.description);
                }
            }}
        >
            <View style={styles.container}>
                <Text style={styles.label}>Perikopa voalahatra*</Text>
                <TextInput 
                    autoFocus={true} 
                    placeholder="soraty eto ny perikopa voalahatra" 
                    style={styles.input}
                    onChangeText={val => setVerses(val)}
                    value={verses}
                />

                <Text style={styles.label}>Fandaharam-potoana</Text>
                <TextInput 
                    placeholder="soraty eto izay fandaharam-potoana anio (tsy voatery)"
                    multiline={true}
                    style={styles.input}
                    onChangeText={val => setDescription(val)}
                    value={description}
                />

                <View>
                    <Button 
                        disabled={!verses || verses === ''} 
                        onPress={() => {
                            if (verses) {
                                if (pericope && pericope.ID) {
                                    const editedPericope: Pericope = {
                                        ID: pericope.ID,
                                        date: todaysDate,
                                        verses,
                                        description
                                    };

                                    updatePericope(editedPericope).then(() => {
                                        onSaved();
                                        modalRef.current?.close();
                                    })
                                } else {
                                    const newPericope: Pericope = {
                                        date: todaysDate,
                                        verses: verses,
                                        description
                                    } 
                                    insertPericope(newPericope).then(() => {
                                        onSaved();
                                        modalRef.current?.close();
                                    });
                                }
                            }
                        }} 
                        title="Enregistrer" 
                    />
                </View>
                <View style={{marginTop: 16}}>
                    <Button onPress={() => modalRef.current?.close()} color="#DD6B55" title="Annuler" />
                </View>
            </View>
        </Modalize>
    )
};

const styles = StyleSheet.create({
    container: {
        margin: 16
    },
    input: {
        /* borderBottomColor: 'black',
        borderBottomWidth: 2,
        borderStyle: 'solid' */
    },
    label: {
        /* position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        fontSize: 10,
        fontWeight: 'bold',
        marginBottom: -15*/
    }
});

export default CRUDModal;
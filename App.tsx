import moment, { Moment } from 'moment';
import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  StatusBar,
  ScrollView,
  Share
} from 'react-native';
import { getWeeksInMonthFromMoment } from './services';
import { initializeLocale } from './services/moment';
import Icon from 'react-native-vector-icons/AntDesign';
import ContentLoader, { Rect } from 'react-content-loader/native';
import { findBYDate, Pericope } from './services/sqlite';
import { Modalize } from 'react-native-modalize';
import CRUDModal from './components/CRUDModal';
import Header from './components/Header';
import { configure, createChannel, localNotificationSchedule } from './services/pushNotification';
import { Constants } from 'react-native-unimodules';

initializeLocale();

configure();

const App = () => {

  const modalizeRef = useRef<Modalize>(null);

  const openCRUDModal = () => modalizeRef.current?.open();

  const [today, setToday] = useState<Date>(new Date());
  const [currentMonthAsMoment, setCurrentMonthAsMoment] = useState<Moment>(moment());
  const [calendar, setCalendar] = useState<Array<Array<Moment>>>([]);
  const [onSearch, setOnSearch] = useState<boolean>(true);
  const [pericope, setPericope] = useState<Pericope>();

  useEffect(() => {

    let dayOfWeek = 0;
    let i = 1;
    let d = dayOfWeek;
    const calendarTabs = [];

    for (i = 1; i <= getWeeksInMonthFromMoment(currentMonthAsMoment); i++) {
      const week = [];
      for (d = dayOfWeek; d < (dayOfWeek + 7); d++) {
        week.push(moment(currentMonthAsMoment.startOf('months')).days(d));
      }

      dayOfWeek = d;
      calendarTabs.push(week);
    }

    findPericope(moment(today));

    setCalendar(calendarTabs);
    
  }, [currentMonthAsMoment]);

  useLayoutEffect(() => {
    createChannel().then(() => localNotificationSchedule());
  }, []);

  const next = () => {
    const month = currentMonthAsMoment.add(1, 'months');
    setCurrentMonthAsMoment(moment(month));
  }

  const previous = () => {
    const month = currentMonthAsMoment.subtract(1, 'months');
    setCurrentMonthAsMoment(moment(month));
  }

  const todayHandler = (today: Moment) => {

    // TODO:
    /*if (moment(today).isBefore(currentMonthAsMoment, 'months')) {
      previous();
    } else if (moment(today).isAfter(currentMonthAsMoment, 'months')) {
      next();
    }*/

    findPericope(today);

    setToday(today.toDate());
  }

  const findPericope = (date: Moment) => {
    setOnSearch(true);

    findBYDate(date)
      .then(items => {
        if (items.length > 0) {
          setPericope(items[0]);
        } else {
          setPericope(undefined);
        }
      })
      .finally(() => setOnSearch(false));
  }

  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
        <ScrollView>
          <Header />
          <View style={[styles.container, styles.container_card]}>
            {
              !onSearch && !pericope && 
              <View>
                <TouchableOpacity onPress={openCRUDModal}>
                  <Text style={{ textAlign: 'center' }}>Tsy mbola tafiditra ny tenin'Andriamanitra ho an'io daty io</Text>
                  <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                    <Icon name="plus" size={20} color="black" />
                  </View>
                </TouchableOpacity>
              </View>
            }
            {
              onSearch &&
              <>
                <View style={styles.todays}>
                  <ContentLoader 
                    speed={2}
                    width={100}
                    height={60}
                    viewBox="0 0 100 60"
                    backgroundColor="#dddddd"
                    foregroundColor="#ecebeb"
                  >
                    <Rect x="20" y="5" rx="8" ry="8" width="60" height="16" /> 
                    <Rect x="5" y="31" rx="8" ry="8" width="92" height="16" />
                  </ContentLoader>
                </View>
                <View style={styles.information}>
                  <ContentLoader 
                    speed={2}
                    width={182}
                    height={77}
                    viewBox="0 0 182 77"
                    backgroundColor="#dddddd"
                    foregroundColor="#ecebeb"
                    style={{width: '100%'}}
                  >
                    <Rect x="1" y="4" rx="3" ry="3" width="140" height="10" /> 
                    <Rect x="1" y="49" rx="3" ry="3" width="154" height="10" /> 
                    <Rect x="2" y="23" rx="3" ry="3" width="90" height="10" /> 
                    <Rect x="97" y="23" rx="3" ry="3" width="58" height="10" /> 
                    <Rect x="1" y="67" rx="3" ry="3" width="151" height="10" />
                  </ContentLoader>
                </View>
              </>
            }
            {
              !onSearch && pericope &&
              <>
                <View style={styles.todays}>
                  <Text style={styles.todays_text}>{ pericope.verses?.split(' ')[0] }</Text>
                  <Text style={styles.todays_text}>{ pericope.verses?.split(' ')[1] }</Text>
                </View>
                <View style={styles.information}>
                  <Text style={styles.information_title}>{moment(today).format('dddd DD/MM/YYYY')}</Text>
                  <Text style={styles.information_text} numberOfLines={4}>
                    {pericope?.description}
                  </Text>
                </View>
                <View style={{position: 'absolute', bottom: 10, right: 20}}>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity onPress={openCRUDModal} style={{marginRight: 20}}>
                      <Icon name="edit" size={20} color="black" />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={async () => {
                      await Share.share({
                        message: `${moment(today).format('dddd DD/MM/YYYY')}: ${pericope?.verses} - ${pericope?.description}`
                      });
                    }}>
                      <Icon name="sharealt" size={20} color="black" />
                    </TouchableOpacity>
                  </View>
                </View>
              </>
            }
          </View>
          <View style={styles.calendar_container}>
            <View style={styles.calendar_month_container}>
              <TouchableOpacity onPress={() => previous()} style={styles.left}>
                <View>
                  <Icon name="left" size={25} />
                </View>
              </TouchableOpacity>
              <Text style={{ textTransform: 'uppercase' }}>{currentMonthAsMoment.format('MMMM YYYY')}</Text>
              <TouchableOpacity onPress={() => next()} style={styles.right}>
                <View>
                  <Icon name="right" size={25} />
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.calendar}>
              {
                moment.weekdaysShort().map((week, w) => (
                  <View key={w} style={styles.calendar_body}>
                    <Text style={{ textTransform: 'uppercase' }}>{week}</Text>
                  </View>
                ))
              }
            </View>
            {
              calendar.map((week, i) => (
                <View key={i} style={styles.calendar}>
                  {
                    week.map((day, i) => (
                      <TouchableOpacity style={[styles.calendar_body, { height: 50 }]} key={i} onPress={() => todayHandler(day)} >
                        <Text style={[
                          moment(today).isSame(day, 'dates') && styles.current_date, 
                          !moment(currentMonthAsMoment).isSame(day, 'months') && styles.another_month]}
                        >
                          {day.date()}
                        </Text>
                      </TouchableOpacity>
                    ))
                  }
                </View>
              ))
            }
          </View>
          <View style={{alignItems: 'center', marginBottom: 50}}>
            <Text style={{textAlign: 'center'}}>v{Constants.nativeAppVersion}</Text>
          </View>
        </ScrollView>
        {/* <TouchableOpacity onPress={() => Alert.alert('ddd')} style={styles.floating_button}>
            <Icon name="plus" size={25} color="black" />
        </TouchableOpacity> */}
        <CRUDModal
          todaysDate={moment(today).format('DD-MM-YYYY')} 
          pericope={pericope} 
          modalRef={modalizeRef}
          onSaved={() => findPericope(moment(today))}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  main_container: {
    marginTop: 20
  },
  container_card: {
    borderRadius: 15,
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 1,
    shadowRadius: 8,
    elevation: 8,
    paddingTop: 16,
    paddingRight: 16,
    paddingLeft: 16,
    paddingBottom: 16
  },
  container: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    margin: 16
  },
  todays: {
    alignItems: 'center',
    backgroundColor: 'cornsilk',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 10,
    padding: 16
  },
  todays_text: {
    fontSize: 30,
    fontWeight: 'bold'
  },
  information: {
    flex: 1,
    marginLeft: 10
  },
  information_title: {
    borderBottomWidth: 1,
    borderStyle: 'solid'
  },
  information_text: {
    paddingTop: 10,
    textTransform: 'uppercase'
  },
  calendar_container: {
    margin: 20
  },
  calendar_month_container: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    marginLeft: 50, 
    marginRight: 50
  },
  left: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flex: 1
  },
  right: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flex: 1
  },
  calendar: {
    flexDirection: 'row',
    marginBottom: 10,
    marginTop: 30,
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    flex: 1
  },
  calendar_body: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  current_date: {
    borderWidth: 1,
    borderStyle: 'solid',
    padding: 5,
    backgroundColor: 'cornsilk'
  },
  calendar_title: {
    textTransform: 'uppercase',
    fontWeight: '700'
  },
  another_month: {
    opacity: .4
  },
  floating_button: {
    borderWidth: 2,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    position: 'absolute',                                          
    bottom: 15,                                                    
    right: 25,
    height: 60,
    backgroundColor: '#fff',
    borderRadius: 100
  }
});

export default App;
